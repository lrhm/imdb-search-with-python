# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from crawler import find_move_by_name
from pathlib import Path
import logging
import subprocess
import os
from parse import *

logging.basicConfig(level=logging.INFO)


def correct_name(name):
    name = name.lower()
    cn = ""
    qualities = ["720", "1080", "brip", "bluray", "dvd", "m-hd", "hd", "web", "brrip", "criterion"
        , "blu-ray", "director's cut", "bdrip", "x264", "hdtv", "xvid", "ntsc"]
    for q in qualities:
        if name.count(q) > 0:
            cn = q
            break
    if len(cn) > 0:
        name = name.split(cn)[0]
    forbidden_list = ["-", ")", "(", "]", "[", "{", "}"]
    name = name.replace(".", " ").replace('_', " ")
    replace = False
    temp = ""
    for j in name:
        if j == '[':
            replace = True

        if not replace and j not in forbidden_list:
            temp += j

        if j == ']':
            replace = False

    name = temp.split(" ")
    index = len(name)
    while index > 0:
        index -= 1
        try:
            year = int(name[index])
            if year > 1900:
                break
        except:
            pass
    if index > 0:
        temp = ""
        for k in range(0, index):
            temp += name[k]
            temp += " "
    return temp


def print_to_file(file, movie):
    print('title : ' + movie['title'] + "\n", file=file)
    print('rating : ' + movie['rating'] + "\n", file=file)
    print('metascore : ' + str(movie['metascore']) + "\n", file=file)
    print('year : ' + movie['year'] + "\n", file=file)
    print('genre : ' + str(movie['genre']) + "\n", file=file)
    print('storyline : ' + movie['storyline'], file=file)
    if "dir" in movie:
        print('dir : ' + movie['dir'] + "\n", file=file)
        print('size : ' + movie['size'] + "\n", file=file)
    print('-------------------------------------\n', file=file)


def du(path):
    """disk usage in human readable format (e.g. '2,1GB')
    :param path :
    """
    return subprocess.check_output(['du', '-s', path]).split()[0].decode('utf-8')


def try_to_find_movie(name):
    try:
        movie = find_move_by_name(name)
        return movie
    except:
        print('error happend , skip this y/n ?')
        x = "s"
        while x != "y" or x != "n":
            x = input()
        if x == "y":
            try_to_find_movie(name)
        else:
            return


